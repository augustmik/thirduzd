package com.example.thirduzd

import androidx.test.core.app.ActivityScenario
import androidx.test.core.app.ActivityScenario.ActivityAction
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.IdlingResource
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.runner.AndroidJUnit4
import com.example.thirduzd.teamInfoScreen.playerList.PlayersListRecyclerViewAdapter
import com.example.thirduzd.teamsListScreen.MainActivity
import com.example.thirduzd.teamsListScreen.MainScreenRecyclerViewAdapter
import org.hamcrest.Matchers.allOf
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class EspressoTest {
    @Test
    fun appLaunchesSuccessfully() {
        ActivityScenario.launch(MainActivity::class.java)
    }

//    @get:Rule
//    var rule = OkHTTPTestRule()

    @Before
    fun init(){
//        = CountingIdlingResource("mainRecycler")
//        IdlingRegistry.getInstance().register(myIdlingResource)
        val activityScenario: ActivityScenario<*> = ActivityScenario.launch(
            MainActivity::class.java
        )
//        val fragScenario : FragmentScenario
//        activityScenario.onActivity( activity ->)

//            ActivityAction<MainActivity> { activity ->
//            val idlingResource = activity.getIdlingResource()
//            // To prove that the test fails, omit this call:
//            IdlingRegistry.getInstance().register(mIdlingResource)
//        })
    }
    @After
    fun done(){
//        IdlingRegistry.getInstance().unregister(myIdlingResource)
    }

    @Test
    fun tabsOpenTest() {
        ActivityScenario.launch(MainActivity::class.java)
//        Thread.sleep(2000L)
        Espresso.
        onView(withId(R.id.recyclerView)).perform(actionOnItemAtPosition<MainScreenRecyclerViewAdapter.MyViewHolder>(0, click()))
        onView(withId(R.id.app_bar_image)).check(matches(isDisplayed()))
    }
    @Test
    fun firstPlayerClickedCorrectly() {
        ActivityScenario.launch(MainActivity::class.java)
//        Thread.sleep(2000L)

        onView(withId(R.id.recyclerView)).perform(actionOnItemAtPosition<MainScreenRecyclerViewAdapter.MyViewHolder>(0, click()))
        onView(withId(R.id.app_bar_image)).perform(swipeUp())
        onView(withId(R.id.vPager)).perform(swipeLeft())
        onView(allOf(withId(R.id.rView), isDisplayed())).perform(actionOnItemAtPosition<PlayersListRecyclerViewAdapter.MyViewHolder>(0, click()))
        onView(withId(R.id.ageTV)).check(matches(withText("25")))
    }
    @Test
    fun secondTeamThrdPlayerClickedCorrectlyFromBack() {
        ActivityScenario.launch(MainActivity::class.java)
//        Thread.sleep(2000L)
//Utils class test actions ClickOn(id paduot)
        onView(withId(R.id.recyclerView)).perform(actionOnItemAtPosition<MainScreenRecyclerViewAdapter.MyViewHolder>(0, click()))
        onView(withId(R.id.app_bar_image)).perform(swipeUp())
        onView(withId(R.id.vPager)).perform(swipeLeft())
        onView(allOf(withId(R.id.rView), isDisplayed())).perform(actionOnItemAtPosition<PlayersListRecyclerViewAdapter.MyViewHolder>(0, click()))
        onView(isRoot()).perform(pressBack())
        onView(isRoot()).perform(pressBack())
        onView(isRoot()).perform(pressBack())
        onView(withId(R.id.recyclerView)).perform(actionOnItemAtPosition<MainScreenRecyclerViewAdapter.MyViewHolder>(1, click()))
        onView(withId(R.id.app_bar_image)).perform(swipeUp())
        onView(withId(R.id.vPager)).perform(swipeLeft())
        onView(allOf(withId(R.id.rView), isDisplayed())).perform(actionOnItemAtPosition<PlayersListRecyclerViewAdapter.MyViewHolder>(2, click()))

        onView(withId(R.id.ageTV)).check(matches(withText("26")))

    }
}