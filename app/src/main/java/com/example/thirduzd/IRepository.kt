package com.example.thirduzd

import androidx.lifecycle.LiveData
import com.example.thirduzd.singleUnits.jsonModules.dbModules.PlayerDB
import com.example.thirduzd.singleUnits.jsonModules.dbModules.ResultsDB
import com.example.thirduzd.singleUnits.jsonModules.dbModules.TeamsDB

interface IRepository {

    fun getNumberOfTeams() : Int

    fun getResults() : LiveData<List<ResultsDB>>
    fun getPlayers() : LiveData<List<PlayerDB>>
    val teamsLiveData : LiveData<List<TeamsDB>>

    suspend fun addPlayersCheck(teamId: Int)
    suspend fun addResultsCheck(teamId: Int)
    suspend fun addTeamsCheck()
}