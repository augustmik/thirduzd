package com.example.thirduzd

import com.example.thirduzd.daggerModules.DaggerRepoComponent
import com.example.thirduzd.daggerModules.RepoModule
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

class MainApplication : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerRepoComponent.builder()
            .repoModule(RepoModule(this))
            .build()
    }


}