package com.example.thirduzd

import androidx.lifecycle.*
import com.example.thirduzd.singleUnits.jsonModules.appViewModelUnits.TeamVM
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class MainViewModel @Inject constructor( private val iRepository: IRepository) : ViewModel() {

    val teamViewModels : LiveData<List<TeamVM>> = Transformations.map(iRepository.teamsLiveData/*getTeamsLive()*/) {
    it.asTeamsVM()
}

    fun numberOfTeamsInList() : Int{
        return iRepository.getNumberOfTeams()
    }
    fun loadTeamsList(){
        viewModelScope.launch(Dispatchers.IO) {
            iRepository.addTeamsCheck()
        }
    }
}
