package com.example.thirduzd

interface OnClickListener {
    fun loadNextActivity(position: Int)
}