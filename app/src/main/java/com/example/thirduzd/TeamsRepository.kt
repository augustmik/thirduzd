package com.example.thirduzd

import com.example.thirduzd.singleUnits.jsonModules.jsonModules.Teams
import android.content.Context
import android.content.SharedPreferences
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.thirduzd.dataBaseTimer.UpdateTimeChecker
import com.example.thirduzd.dbRoom.RoomDBService
import com.example.thirduzd.dataBaseTimer.UpdateTimeEnum
import com.example.thirduzd.singleUnits.jsonModules.appViewModelUnits.PlayerVM
import com.example.thirduzd.singleUnits.jsonModules.appViewModelUnits.TeamVM
import com.example.thirduzd.singleUnits.jsonModules.dbModules.PlayerDB
import com.example.thirduzd.singleUnits.jsonModules.dbModules.ResultsDB
import com.example.thirduzd.singleUnits.jsonModules.dbModules.TeamsDB
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TeamsRepository @Inject constructor(private val roomDBService:RoomDBService, private val updateTime : UpdateTimeChecker) : IRepository {

    private var _listOfTeams: MutableList<Teams> = mutableListOf()

    lateinit var loadedTeam : TeamVM //loadedItems
    lateinit var loadedPlayer: PlayerVM

    private var _teamsLiveData: MutableLiveData<List<TeamsDB>> = MutableLiveData()

    private var _resultsLiveData: MutableLiveData<List<ResultsDB>> = MutableLiveData()

    private var _playersLiveData: MutableLiveData<List<PlayerDB>> = MutableLiveData()

//    private object Holder {
//        val INSTANCE = TeamsRepository()
//    }
//    companion object {
//        val instance: TeamsRepository by lazy { Holder.INSTANCE }
//    }
    override fun getResults() = _resultsLiveData
    override fun getPlayers() = _playersLiveData

    override val teamsLiveData  : LiveData<List<TeamsDB>>
    get() = _teamsLiveData



    override suspend fun addTeamsCheck() {
        resetTeams()
        if (updateTime.updateTeamsList()) {
            _teamsLiveData.postValue(roomDBService.renewTeams())
        } else _teamsLiveData.postValue(roomDBService.loadTeamsFromDB())

    }

    override suspend fun addPlayersCheck(teamId: Int) {
        val teamName = loadedTeam.strTeam
        if ( updateTime.updatePlayersList(teamId)) {
            _playersLiveData.postValue(roomDBService.renewPlayers(teamName, teamId))
        } else _playersLiveData.postValue(roomDBService.loadPlayersFromDB(teamId))
    }

    override suspend fun addResultsCheck(teamId: Int) {
        val teamIdReal = loadedTeam.idTeam
        if ( updateTime.updateNewsList(teamIdReal)) {
            _resultsLiveData.postValue(roomDBService.renewResults(teamIdReal))
        } else _resultsLiveData.postValue(roomDBService.loadResultsFromDB(teamIdReal))
    }

    override fun getNumberOfTeams(): Int {
        return _listOfTeams.size
    }



    fun resetTeams() {
        _teamsLiveData.postValue(listOf())
    }

    fun resetResults() {
        _resultsLiveData.postValue(listOf())
    }

    fun resetPlayers() {
        _playersLiveData.postValue(listOf())
    }

}