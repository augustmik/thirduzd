package com.example.thirduzd

import com.example.thirduzd.singleUnits.jsonModules.appViewModelUnits.PlayerVM
import com.example.thirduzd.singleUnits.jsonModules.appViewModelUnits.ResultsVM
import com.example.thirduzd.singleUnits.jsonModules.appViewModelUnits.TeamVM
import com.example.thirduzd.singleUnits.jsonModules.jsonModules.Player
import com.example.thirduzd.singleUnits.jsonModules.jsonModules.Results
import com.example.thirduzd.singleUnits.jsonModules.jsonModules.Teams
import com.example.thirduzd.singleUnits.jsonModules.dbModules.PlayerDB
import com.example.thirduzd.singleUnits.jsonModules.dbModules.ResultsDB
import com.example.thirduzd.singleUnits.jsonModules.dbModules.TeamsDB
import java.time.LocalDate
import java.time.Period


fun List<Teams>.asTeamsDB() : List<TeamsDB>{
    return this.map {
        TeamsDB(
            it.idTeam,
            it.strTeam,
            it.strTeamShort,
            it.strTeamBadge,
            it.strStadiumThumb,
            it.strDescriptionEN,
            it.strTeamLogo
        )
    }
}

fun List<Results>.asResultsDB(teamId : Int) : List<ResultsDB>{
    return this.map {
        ResultsDB(
            it.idEvent,
            teamId,
            it.strEvent,
            it.strDescriptionEN,
            it.strHomeTeam,
            it.strAwayTeam,
            it.strDate,
            it.strTime,
            it.strThumb
        )
    }
}
fun List<Player>.asPlayerDB() : List<PlayerDB>{
    return this.map {
        PlayerDB(
            it.idPlayer,
            it.idTeam,
            it.strPlayer,
            it.dateBorn,
            it.strDescriptionEN,
            it.strPosition,
            it.strHeight,
            it.strWeight,
            it.strThumb
        )
    }
}
fun List<TeamsDB>.asTeamsVM() : List<TeamVM>{
    return this.map {
        TeamVM(
            it.idTeam!!,
            it.strTeam!!,
            it.strTeamShort!!,
            it.strTeamBadge!!,
            it.strStadiumThumb!!,
            it.strDescriptionEN!!,
            it.strTeamLogo!!
        )
    }
}
fun List<ResultsDB>.asResultsVM() : List<ResultsVM>{
    return this.map {
        ResultsVM(
            it.idEvent ?: 0 ,
            it.idTeam ?: 0,
            it.strEvent ?: "",
            it.strDescriptionEN ?: "",
            it.strHomeTeam ?: "",
            it.strAwayTeam ?: "",
            "November 10, 1996",
            it.strTime ?: "",
            "Thumb"//it.strThumb!!
        )
    }
}
fun List<PlayerDB>.asPlayerVM() : List<PlayerVM>{
    return this.map {
        PlayerVM(
            it.idPlayer ?: -1,
            it.idTeam ?: -1,
            it.strPlayer ?: "",
            convertDOBtoAge(it.dateBorn!!),
            //it.dateBorn ?: "",
            it.strDescriptionEN ?: "",
            it.strPosition ?: "",
            it.strHeight ?: "",
            it.strWeight ?: "",
            it.strThumb ?: ""
        )
    }
}

fun convertDOBtoAge (dob : String) : String{
    val dateOfBirth = LocalDate.parse(dob)
    val current = LocalDate.now()

    return Period.between(dateOfBirth, current).years.toString()
}