package com.example.thirduzd.daggerModules

import com.example.thirduzd.playerInfoScreen.ThrdScreenActivity
import com.example.thirduzd.teamInfoScreen.SecondScreenViewPagerActivity
import com.example.thirduzd.teamInfoScreen.newsList.NewsListFragment
import com.example.thirduzd.teamInfoScreen.playerList.PlayerListFragment
import com.example.thirduzd.teamsListScreen.MainFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class AndroidInjectorBuilder {

    @ContributesAndroidInjector
    abstract fun bindMainFragment() : MainFragment

    @ContributesAndroidInjector
    abstract fun bindNewsFragment() : NewsListFragment

    @ContributesAndroidInjector
    abstract fun bindPlayersFragment() : PlayerListFragment

    @ContributesAndroidInjector
    abstract fun bindPlayerInfoActivity() : ThrdScreenActivity

    @ContributesAndroidInjector
    abstract fun bindTeamInfoActivity() : SecondScreenViewPagerActivity

}