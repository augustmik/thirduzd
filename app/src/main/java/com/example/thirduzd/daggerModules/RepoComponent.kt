package com.example.thirduzd.daggerModules

import com.example.thirduzd.MainApplication
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component( modules = [
    AndroidSupportInjectionModule::class,
    AndroidInjectorBuilder::class,

    RepoModule::class,
    ViewModelModule::class,
    ViewModelFactoryModule::class
    //    ContextHolder::class
    ])

interface RepoComponent : AndroidInjector<MainApplication>