package com.example.thirduzd.daggerModules

import android.content.Context
import android.content.SharedPreferences
import com.example.thirduzd.IRepository
import com.example.thirduzd.TeamsRepository
import com.example.thirduzd.dataBaseTimer.UpdateTimeChecker
import com.example.thirduzd.dbRoom.RoomDBService
import com.example.thirduzd.retrofit.IWebService
import com.example.thirduzd.retrofit.RetroNetwork
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepoModule (val context: Context) {

    @Provides @Singleton
    fun provideRepository(roomDBService: RoomDBService, updateTimeChecker: UpdateTimeChecker) : TeamsRepository = TeamsRepository(roomDBService, updateTimeChecker)

    @Provides @Singleton
    fun provideRepositoryInterface(teamsRepo: TeamsRepository) : IRepository = teamsRepo

    @Provides @Singleton
    fun providesRoomDBService(retroS : IWebService): RoomDBService = RoomDBService(context, retroS)

    @Provides @Singleton
    fun providesRetro() : IWebService = RetroNetwork.rService

    @Provides @Singleton
    fun providesDBUpdateTime(sharedPref: SharedPreferences) : UpdateTimeChecker = UpdateTimeChecker(sharedPref)

    @Provides
    fun providesSharedPref() : SharedPreferences = context.getSharedPreferences("MyPref", 0)
}