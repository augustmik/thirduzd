package com.example.thirduzd.daggerModules

import androidx.lifecycle.ViewModelProvider
import com.example.thirduzd.daggerModules.ViewModelFactory
import dagger.Binds
import dagger.Module

@Module
abstract class ViewModelFactoryModule {
    @Binds
    abstract fun bindViewModelFactory(viewModelFactory: ViewModelFactory): ViewModelProvider.Factory
}