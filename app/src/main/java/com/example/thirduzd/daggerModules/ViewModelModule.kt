package com.example.thirduzd.daggerModules

import androidx.lifecycle.ViewModel
import com.example.thirduzd.MainViewModel
import com.example.thirduzd.teamInfoScreen.newsList.NewsListFragmentViewModel
import com.example.thirduzd.teamInfoScreen.playerList.PlayerListFragmentViewModel
import dagger.Binds
import dagger.MapKey
import dagger.Module
import dagger.multibindings.IntoMap
import kotlin.reflect.KClass

@Module
abstract class ViewModelModule {
    @Binds @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun bindMainViewModel(customViewModel:  MainViewModel): ViewModel

    @Binds @IntoMap
    @ViewModelKey(NewsListFragmentViewModel::class)
    abstract fun bindNewsViewModel(customViewModel:  NewsListFragmentViewModel): ViewModel

    @Binds @IntoMap
    @ViewModelKey(PlayerListFragmentViewModel::class)
    abstract fun bindPlayerListViewModel(customViewModel: PlayerListFragmentViewModel): ViewModel


}

@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER)
@MapKey
annotation class ViewModelKey(val value: KClass<out ViewModel>)