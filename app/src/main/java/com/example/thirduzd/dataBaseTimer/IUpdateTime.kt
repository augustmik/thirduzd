package com.example.thirduzd.dataBaseTimer

interface IUpdateTime {
    fun getUpdateTime() : Long
}