package com.example.thirduzd.dataBaseTimer

import android.content.SharedPreferences
import javax.inject.Inject

class UpdateTimeChecker @Inject constructor(private val pref: SharedPreferences) {

    private val editor: SharedPreferences.Editor = pref.edit()

    fun updateTeamsList() : Boolean{
        val cTime = System.currentTimeMillis()
        val timePassed = cTime - pref.getLong("teamsUpdate", -1L)
        var updateStatus : Boolean = false

        if (timePassed >= UpdateTimeEnum.Team.getUpdateTime()){
            updateStatus = true
            editor.putLong("teamsUpdate", cTime)
            editor.commit()
        }

        return updateStatus
    }
    fun updatePlayersList( teamId : Int) : Boolean{
        val cTime = System.currentTimeMillis()
        val timePassed = cTime - pref.getLong("playersUpdate$teamId", -1L)
        var updateStatus : Boolean = false

        if (timePassed >= UpdateTimeEnum.Player.getUpdateTime()){
            updateStatus = true
            editor.putLong("playersUpdate$teamId", cTime)
            editor.commit()
        }

        return updateStatus
    }
    fun updateNewsList( teamId: Int) : Boolean{
        val cTime = System.currentTimeMillis()
        val timePassed = cTime - pref.getLong("resultsUpdate$teamId", -1L)
        var updateStatus : Boolean = false

        if (timePassed >= UpdateTimeEnum.Events.getUpdateTime()){
            updateStatus = true
            editor.putLong("resultsUpdate$teamId", cTime)
            editor.commit()
        }

        return updateStatus
    }
}