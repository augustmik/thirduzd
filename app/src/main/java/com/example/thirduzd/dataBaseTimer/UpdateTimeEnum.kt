package com.example.thirduzd.dataBaseTimer

enum class UpdateTimeEnum : IUpdateTime{
    Team{ override fun getUpdateTime() = 3600000L },
    Events{ override fun getUpdateTime() = 900000L },
    Player{ override fun getUpdateTime() = 1800000L }
}