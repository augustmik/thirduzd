package com.example.thirduzd.dbRoom

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.thirduzd.singleUnits.jsonModules.dbModules.PlayerDB
import com.example.thirduzd.singleUnits.jsonModules.dbModules.ResultsDB
import com.example.thirduzd.singleUnits.jsonModules.dbModules.TeamsDB

@Database(entities = [TeamsDB::class, PlayerDB::class, ResultsDB::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun teamsDao(): TeamsDao
    abstract fun playersDao(): PlayersDao
    abstract fun resultsDao(): ResultsDao
}