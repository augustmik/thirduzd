package com.example.thirduzd.dbRoom

import androidx.room.*
import com.example.thirduzd.singleUnits.jsonModules.dbModules.PlayerDB

@Dao
interface PlayersDao {
    @Query("SELECT * FROM Players")
    fun getAll(): List<PlayerDB>

    @Query("SELECT * FROM Players WHERE idTeam = (:teamId)")
    fun loadAllByTeamId(teamId: Int): List<PlayerDB>

    @Insert (onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(players: List<PlayerDB>)

    @Delete
    fun delete(user: PlayerDB)
}