package com.example.thirduzd.dbRoom

import androidx.room.*
import com.example.thirduzd.singleUnits.jsonModules.dbModules.ResultsDB

@Dao
interface ResultsDao {
    @Query("SELECT * FROM Events")
    fun getAll(): List<ResultsDB>

    @Query("SELECT * FROM Events WHERE idTeam = (:teamId)")
    fun loadAllByTeamId(teamId: Int): List<ResultsDB>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(events: List<ResultsDB>)

    @Delete
    fun delete(event: ResultsDB)
}