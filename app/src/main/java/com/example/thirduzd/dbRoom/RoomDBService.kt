package com.example.thirduzd.dbRoom

import android.content.Context
import androidx.room.Room
import com.example.thirduzd.singleUnits.jsonModules.dbModules.PlayerDB
import com.example.thirduzd.singleUnits.jsonModules.dbModules.ResultsDB
import com.example.thirduzd.singleUnits.jsonModules.dbModules.TeamsDB
import com.example.thirduzd.asPlayerDB
import com.example.thirduzd.asResultsDB
import com.example.thirduzd.asTeamsDB
import com.example.thirduzd.retrofit.IWebService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RoomDBService @Inject constructor(context: Context, val rService: IWebService){
    val db = Room.databaseBuilder(
        context,
        AppDatabase::class.java, "nba_info_db"
    ).build()

    suspend fun renewTeams() : List<TeamsDB> {

        val teamList = rService.getTeams()
        val teamsDB = teamList.teams.asTeamsDB()
        db.teamsDao().insertAll(teamsDB)
        return db.teamsDao().getAll()
    }
    fun loadTeamsFromDB() = db.teamsDao().getAll()

    suspend fun renewResults(teamId : Int) : List<ResultsDB> {

        val resultsList = rService.getResults(teamId)
        val resultsDB = resultsList.results.asResultsDB(teamId)
        db.resultsDao().insertAll(resultsDB)

        return db.resultsDao().loadAllByTeamId(teamId)
    }
    fun loadResultsFromDB(teamID : Int) = db.resultsDao().loadAllByTeamId(teamID)

    suspend fun renewPlayers(teamName : String, teamID: Int) : List<PlayerDB> {

        val playerList = rService.getPlayers(teamName)
        val playersDB = playerList.player.asPlayerDB()
        db.playersDao().insertAll(playersDB)
        return db.playersDao().loadAllByTeamId(teamID)
    }
    fun loadPlayersFromDB(teamID : Int) = db.playersDao().loadAllByTeamId(teamID)
}