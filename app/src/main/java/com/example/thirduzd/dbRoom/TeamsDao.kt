package com.example.thirduzd.dbRoom

import androidx.room.*
import com.example.thirduzd.singleUnits.jsonModules.dbModules.TeamsDB

@Dao
interface TeamsDao {

    @Query("SELECT * FROM Teams")
    fun getAll(): List<TeamsDB>//List<TeamsDB>

    @Insert (onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(teams: List<TeamsDB>)

    @Delete
    fun delete(team: TeamsDB)
}