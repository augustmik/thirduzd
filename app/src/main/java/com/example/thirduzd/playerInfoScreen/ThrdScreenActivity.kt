package com.example.thirduzd.playerInfoScreen

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.thirduzd.TeamsRepository
import com.example.thirduzd.databinding.PlayerScreenBinding
import com.squareup.picasso.Picasso
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

class ThrdScreenActivity : DaggerAppCompatActivity() {

    private lateinit var _mainBinding : PlayerScreenBinding
    @Inject
    lateinit var repository: TeamsRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _mainBinding = PlayerScreenBinding.inflate(layoutInflater)
        val player = repository.loadedPlayer
        _mainBinding.vm = player
        if(player.strThumb != "") {Picasso.get().load(player.strThumb).into(_mainBinding.appBarImage)}

        setContentView(_mainBinding.root)
    }
}
