package com.example.thirduzd.retrofit

import com.example.thirduzd.singleUnits.jsonModules.jsonModules.PlayerList
import com.example.thirduzd.singleUnits.jsonModules.jsonModules.ResultsList
import com.example.thirduzd.singleUnits.jsonModules.jsonModules.TeamsList
import retrofit2.http.GET
import retrofit2.http.Query

interface IWebService {

    @GET("/api/v1/json/1/lookup_all_teams.php/?id=4387")
    suspend fun getTeams () : TeamsList

    @GET("/api/v1/json/1/searchplayers.php")
    suspend fun getPlayers (@Query("t") teamName : String? ) : PlayerList

    @GET("/api/v1/json/1/eventslast.php")
    suspend fun getResults (@Query("id") teamId : Int ) : ResultsList
}