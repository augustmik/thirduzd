package com.example.thirduzd.retrofit

import com.google.gson.GsonBuilder
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetroNetwork : IRetro {
    private val gson = GsonBuilder()
        .setLenient()
        .create()

    private var retrofit: Retrofit = Retrofit.Builder()
        .baseUrl("https://www.thesportsdb.com")
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build()

    override val rService: IWebService = retrofit.create(IWebService::class.java)
}