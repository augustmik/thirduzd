package com.example.thirduzd.singleUnits.jsonModules.appViewModelUnits

import androidx.lifecycle.ViewModel

class PlayerVM (
        val idPlayer: Int,
        val idTeam: Int,
        val strPlayer: String,
        val dateBorn: String,
        val strDescriptionEN: String?,
        val strPosition: String,
        val strHeight: String,
        val strWeight: String,
        val strThumb: String?
) : ViewModel()

