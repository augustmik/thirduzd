package com.example.thirduzd.singleUnits.jsonModules.appViewModelUnits

import androidx.lifecycle.ViewModel

class ResultsVM (
    val idEvent : Int,
    val idTeam : Int,
    val strEvent : String,
    val strDescriptionEN : String?,
    val strHomeTeam : String,
    val strAwayTeam : String,
    val strDate : String,
    val strTime : String,
    val strThumb : String?
) : ViewModel() {}