package com.example.thirduzd.singleUnits.jsonModules.appViewModelUnits

import androidx.lifecycle.ViewModel

class TeamVM (
    val idTeam : Int,
    val strTeam : String,
    val strTeamShort : String,
    val strTeamBadge : String,
    val strStadiumThumb : String,
    val strDescriptionEN : String,
    val strTeamLogo : String
) : ViewModel() {}