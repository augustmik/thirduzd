package com.example.thirduzd.singleUnits.jsonModules.dbModules

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Players")
data class PlayerDB (
    @field:PrimaryKey
    val idPlayer : Int?,

    @field:ColumnInfo(name = "idTeam")
    val idTeam : Int?,

    @field:ColumnInfo(name = "strPlayer")
    val strPlayer : String?,

    @field:ColumnInfo(name = "dateBorn")
    val dateBorn : String?,

    @field:ColumnInfo(name = "strDescriptionEN")
    val strDescriptionEN : String?,

    @field:ColumnInfo(name = "strPosition")
    val strPosition : String?,

    @field:ColumnInfo(name = "strHeight")
    val strHeight : String?,

    @field:ColumnInfo(name = "strWeight")
    val strWeight : String?,

    @field:ColumnInfo(name = "strThumb")
    val strThumb : String?
    )