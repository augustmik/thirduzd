package com.example.thirduzd.singleUnits.jsonModules.dbModules

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Events")
data class ResultsDB (

    @field:PrimaryKey
    val idEvent : Int?,

    @field:ColumnInfo(name = "idTeam")
    val idTeam : Int?,

    @field:ColumnInfo(name = "strEvent")
    val strEvent : String?,

    @field:ColumnInfo(name = "strDescriptionEN")
    val strDescriptionEN : String?,

    @field:ColumnInfo(name = "strHomeTeam")
    val strHomeTeam : String?,

    @field:ColumnInfo(name = "strAwayTeam")
    val strAwayTeam : String?,

    @field:ColumnInfo(name = "strDate")
    val strDate : String?,

    @field:ColumnInfo(name = "strTime")
    val strTime : String?,

    @field:ColumnInfo(name = "strThumb")
    val strThumb : String?
    )
