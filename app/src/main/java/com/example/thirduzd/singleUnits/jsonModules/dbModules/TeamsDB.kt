package com.example.thirduzd.singleUnits.jsonModules.dbModules

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "Teams")
data class TeamsDB (
    @field:PrimaryKey
    val idTeam : Int?,

    @field:ColumnInfo(name = "strTeam")
    val strTeam : String?,

    @field:ColumnInfo(name = "strTeamShort")
    val strTeamShort : String?,

    @field:ColumnInfo(name = "strTeamBadge")
    val strTeamBadge : String?,

    @field:ColumnInfo(name = "strStadiumThumb")
    val strStadiumThumb : String?,

    @field:ColumnInfo(name = "strDescriptionEN")
    val strDescriptionEN : String?,

    @field:ColumnInfo(name = "strTeamLogo")
    val strTeamLogo : String?
)