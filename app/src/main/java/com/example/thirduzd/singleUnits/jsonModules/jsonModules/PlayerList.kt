package com.example.thirduzd.singleUnits.jsonModules.jsonModules

data class PlayerList (
    val player : List<Player>
)