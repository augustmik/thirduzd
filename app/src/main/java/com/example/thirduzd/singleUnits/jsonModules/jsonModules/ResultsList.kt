package com.example.thirduzd.singleUnits.jsonModules.jsonModules

data class ResultsList (
    val results : List<Results>
)
