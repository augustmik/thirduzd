package com.example.thirduzd.singleUnits.jsonModules.jsonModules

data class TeamsList (
    val teams: List<Teams>
)