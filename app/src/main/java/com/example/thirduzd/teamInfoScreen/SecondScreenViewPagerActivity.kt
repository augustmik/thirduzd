package com.example.thirduzd.teamInfoScreen


import android.os.Bundle
import androidx.databinding.DataBindingUtil.setContentView
import androidx.fragment.app.Fragment

import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.ViewPager
import com.example.thirduzd.teamInfoScreen.newsList.NewsListFragment
import com.example.thirduzd.teamInfoScreen.playerList.PlayerListFragment
import com.example.thirduzd.TeamsRepository
import com.example.thirduzd.databinding.ActivitySecondBinding
import com.example.thirduzd.singleUnits.jsonModules.appViewModelUnits.TeamVM
import com.squareup.picasso.Picasso
import dagger.android.support.DaggerAppCompatActivity
import java.lang.annotation.Inherited
import javax.inject.Inject

private const val NUM_PAGES = 2
private var sentId : Int = -1
private lateinit var binding : ActivitySecondBinding


class SecondScreenViewPagerActivity : DaggerAppCompatActivity() {

    private lateinit var mPager: ViewPager

    @Inject
    lateinit var repository: TeamsRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySecondBinding.inflate(layoutInflater)
        setContentView(binding.root)
        sentId = intent.getIntExtra("teamClicked",-1)
        mPager = binding.vPager
        val pagerAdapter = ScreenSlidePagerAdapter(supportFragmentManager)
        mPager.adapter = pagerAdapter

        Picasso.get().load(repository.loadedTeam.strStadiumThumb).into(binding.appBarImage)

    }

    override fun onBackPressed() {
        if (mPager.currentItem == 0) {
            // If the user is currently looking at the first step, allow the system to handle the
            // Back button. This calls finish() on this activity and pops the back stack.
//            TeamsRepository.instance.resetPlayers()
//            TeamsRepository.instance.resetResults()
            super.onBackPressed()
        } else {
            // Otherwise, select the previous step.
            mPager.currentItem = mPager.currentItem - 1
        }
    }
    private inner class ScreenSlidePagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {
        override fun getCount(): Int =
            NUM_PAGES

        override fun getItem(position: Int): Fragment = when (position){
            0 -> NewsListFragment(
                sentId
            )
            1 -> PlayerListFragment(
                sentId
            )
            else -> PlayerListFragment(
                sentId
            )
        }

        override fun getPageTitle(position: Int): CharSequence? {
            lateinit var value : String
            when (position){
                0 -> value = "News"
                1 -> value =  "Players"
                else -> value =  "error"
            }
            return value
        }
    }

}