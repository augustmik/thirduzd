package com.example.thirduzd.teamInfoScreen.newsList

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.thirduzd.TeamsRepository
import com.example.thirduzd.daggerModules.ViewModelFactory
import com.example.thirduzd.databinding.FragmentNewsListBinding
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class NewsListFragment (private val mID: Int): DaggerFragment() {

    @Inject lateinit var vmFactory : ViewModelFactory

    lateinit var binding: FragmentNewsListBinding
    private val mAdapter =
        NewsListRecyclerViewAdapter(
            mutableListOf()
        )

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View{
        binding = FragmentNewsListBinding.inflate(inflater, container, false)

        binding.rView.apply {
            layoutManager = LinearLayoutManager(activity)
            adapter = mAdapter
        }
        return binding.root
    }
    @Inject
    lateinit var repository: TeamsRepository
    override fun onStart() {
        super.onStart()
        val viewModel = ViewModelProviders.of(this,
            vmFactory
        ).get(NewsListFragmentViewModel::class.java)

        viewModel.loadNewsList(mID)
        viewModel.repoResults.observe(this, Observer { results ->
            mAdapter.renewList(results)})
    }

}