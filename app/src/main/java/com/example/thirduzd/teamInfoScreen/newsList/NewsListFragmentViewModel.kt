package com.example.thirduzd.teamInfoScreen.newsList

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.thirduzd.IRepository
import com.example.thirduzd.asResultsVM
import com.example.thirduzd.singleUnits.jsonModules.appViewModelUnits.ResultsVM
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class NewsListFragmentViewModel @Inject constructor (private val iRepository: IRepository) : ViewModel() {
    val repoResults : LiveData<List<ResultsVM>> = Transformations.map(iRepository.getResults()) {
        it.asResultsVM()
    }

    fun loadNewsList(id : Int){
        viewModelScope.launch(Dispatchers.IO) {
            iRepository.addResultsCheck(id)
        }
    }
}