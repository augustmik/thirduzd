package com.example.thirduzd.teamInfoScreen.newsList

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.thirduzd.R
import com.example.thirduzd.databinding.FragmentSingleNewsInListBinding
import com.example.thirduzd.singleUnits.jsonModules.appViewModelUnits.ResultsVM

class NewsListRecyclerViewAdapter (private var myDataset: MutableList<ResultsVM>) :
    RecyclerView.Adapter<NewsListRecyclerViewAdapter.MyViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val myView = DataBindingUtil.inflate<FragmentSingleNewsInListBinding>(
            LayoutInflater.from(parent.context),
            R.layout.fragment_single_news_in_list,
            parent,
            false
        )
        return MyViewHolder(
            myView
        )
    }

    override fun getItemCount() = myDataset.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(myDataset[position])
    }

    class MyViewHolder(val binding: FragmentSingleNewsInListBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(result: ResultsVM) {
            binding.vm = result
        }
    }

    fun renewList(items: List<ResultsVM>) {
        myDataset.clear()
        myDataset.addAll(items)
        notifyDataSetChanged()
    }
}