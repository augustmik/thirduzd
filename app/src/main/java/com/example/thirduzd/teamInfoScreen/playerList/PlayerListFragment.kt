package com.example.thirduzd.teamInfoScreen.playerList

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.thirduzd.*
import com.example.thirduzd.daggerModules.ViewModelFactory
import com.example.thirduzd.playerInfoScreen.ThrdScreenActivity
import com.example.thirduzd.databinding.FragmentPlayerListBinding
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class PlayerListFragment (private val mID: Int): DaggerFragment(), OnClickListener {

    @Inject lateinit var repository: TeamsRepository
    @Inject lateinit var vmFactory : ViewModelFactory

    lateinit var binding: FragmentPlayerListBinding
    private lateinit var mAdapter : PlayersListRecyclerViewAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentPlayerListBinding.inflate(inflater, container, false)
        mAdapter =
            PlayersListRecyclerViewAdapter(
                mutableListOf(),
                this,
                repository
            )
        binding.rView.apply {
            layoutManager = LinearLayoutManager(activity)
            adapter = mAdapter
        }

        return binding.root
    }


    override fun onStart() {

        val viewModel = ViewModelProviders.of(this,
            vmFactory
        ).get(PlayerListFragmentViewModel::class.java)

        viewModel.loadPlayersList(mID)
        viewModel.repoResults.observe(this, Observer { player ->
            mAdapter.renewList(player)})
        super.onStart()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)


    }

    override fun loadNextActivity(position: Int) {
        Log.i("playerItem", position.toString())
        val intent = Intent(activity, ThrdScreenActivity::class.java)

        intent.putExtra("playerClicked", position)
        intent.putExtra("positionClicked", mID)
        startActivity(intent)

    }

}