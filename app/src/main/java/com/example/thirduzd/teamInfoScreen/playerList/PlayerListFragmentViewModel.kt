package com.example.thirduzd.teamInfoScreen.playerList

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.thirduzd.IRepository
import com.example.thirduzd.asPlayerVM
import com.example.thirduzd.singleUnits.jsonModules.appViewModelUnits.PlayerVM
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class PlayerListFragmentViewModel @Inject constructor(private val iRepository: IRepository) : ViewModel() {
    val repoResults : LiveData<List<PlayerVM>> = Transformations.map(iRepository.getPlayers()){
        it.asPlayerVM()
    }

    fun loadPlayersList(id : Int){
        viewModelScope.launch(Dispatchers.IO) {
            iRepository.addPlayersCheck(id)
        }
    }
}