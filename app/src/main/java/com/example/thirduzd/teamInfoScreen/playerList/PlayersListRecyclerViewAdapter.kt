package com.example.thirduzd.teamInfoScreen.playerList

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.thirduzd.OnClickListener
import com.example.thirduzd.R
import com.example.thirduzd.TeamsRepository
import com.example.thirduzd.databinding.FragmentSinglePlayerInListBinding
import com.example.thirduzd.singleUnits.jsonModules.appViewModelUnits.PlayerVM
import com.squareup.picasso.Picasso

class PlayersListRecyclerViewAdapter (private var myDataset: MutableList<PlayerVM>, private var clickListener: OnClickListener, private var repo: TeamsRepository) :
    RecyclerView.Adapter<PlayersListRecyclerViewAdapter.MyViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val myView = DataBindingUtil.inflate<FragmentSinglePlayerInListBinding>(
            LayoutInflater.from(parent.context),
            R.layout.fragment_single_player_in_list,
            parent,
            false
        )
        return MyViewHolder(
            myView
        )
    }

    override fun getItemCount() = myDataset.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(myDataset[position])
        holder.binding.mainCv.setOnClickListener {
            repo.loadedPlayer = myDataset[position]
            clickListener.loadNextActivity(position)
        }
    }

    class MyViewHolder(val binding: FragmentSinglePlayerInListBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(player: PlayerVM) {
            binding.playerInfo.text = player.strPlayer + ", " + player.strPosition
            if(player.strThumb != ""){
            Picasso.get().load(player.strThumb).into(binding.playerPic)}
        }
    }

    fun renewList(items: List<PlayerVM>) {
        myDataset.clear()
        myDataset.addAll(items)
        notifyDataSetChanged()
    }
}