package com.example.thirduzd.teamsListScreen

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.Nullable
import androidx.annotation.VisibleForTesting
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.test.espresso.IdlingResource
import androidx.test.espresso.idling.CountingIdlingResource
import com.example.thirduzd.*
import com.example.thirduzd.daggerModules.ViewModelFactory
import com.example.thirduzd.teamInfoScreen.SecondScreenViewPagerActivity
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_main.*
import javax.inject.Inject


class MainFragment : OnClickListener, DaggerFragment() {

    @Nullable
    private var idlingResource: CountingIdlingResource? = null

    @Inject lateinit var vmFactory : ViewModelFactory
    @Inject lateinit var repository: TeamsRepository

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val viewModel = ViewModelProviders.of(this,
            vmFactory
        ).get(MainViewModel::class.java)

        viewModel.loadTeamsList()

        val mAdapter =
            MainScreenRecyclerViewAdapter(
                mutableListOf(),
                this,
                repository
            )
        recyclerView.apply{
            layoutManager = LinearLayoutManager(activity)
            adapter = mAdapter
        }
        viewModel.teamViewModels.observe(viewLifecycleOwner, Observer { teams ->
            idlingResource?.increment()
            mAdapter.renewList(teams)
            idlingResource?.decrement()
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun loadNextActivity(position: Int) {
        Log.i("TeamItemClicked", position.toString())
        val intent = Intent(activity, SecondScreenViewPagerActivity::class.java)
        intent.putExtra("teamClicked", position)
        startActivity(intent)
    }

    @VisibleForTesting
    fun getIdlingResource(): IdlingResource {
        if (idlingResource == null) {
            idlingResource = CountingIdlingResource("MainFragmentIdling")
        }
        return idlingResource as CountingIdlingResource
    }
}
