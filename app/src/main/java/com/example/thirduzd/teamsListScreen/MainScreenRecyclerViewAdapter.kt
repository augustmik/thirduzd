package com.example.thirduzd.teamsListScreen

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.idling.CountingIdlingResource
import com.example.thirduzd.OnClickListener
import com.example.thirduzd.R
import com.example.thirduzd.TeamsRepository
import com.example.thirduzd.singleUnits.jsonModules.appViewModelUnits.TeamVM
import com.example.thirduzd.databinding.SingleRecyclerviewUnitBinding
import com.squareup.picasso.Picasso

class MainScreenRecyclerViewAdapter(private var myDataset: MutableList<TeamVM>, private var clickListener: OnClickListener, private var repo: TeamsRepository) :
    RecyclerView.Adapter<MainScreenRecyclerViewAdapter.MyViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val myView = DataBindingUtil.inflate<SingleRecyclerviewUnitBinding>(
            LayoutInflater.from(parent.context),
            R.layout.single_recyclerview_unit,
            parent,
            false
        )
        return MyViewHolder(
            myView
        )
    }

    override fun getItemCount() = myDataset.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(myDataset[position])

        holder.binding.mainCv.setOnClickListener {
            val realId : Int = myDataset[position].idTeam
            repo.loadedTeam = myDataset[position]
            clickListener.loadNextActivity(realId)
        }
    }

    class MyViewHolder(val binding: SingleRecyclerviewUnitBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(team: TeamVM) {
            binding.vm = team
            Picasso.get().load(team.strTeamLogo).into(binding.imageView)
        }

    }

    fun renewList(items: List<TeamVM>) {
        myDataset.clear()
        myDataset.addAll(items)
        notifyDataSetChanged()
    }
}