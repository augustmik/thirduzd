package com.example.thirduzd

import android.content.Context
import android.content.SharedPreferences
import com.example.thirduzd.dataBaseTimer.UpdateTimeChecker
import junit.framework.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock


@RunWith(JUnit4::class)//@RunWith(MockitoJUnitRunner::class)
class MockedRepoViewModelTests {

    @Test
    fun testTeamsUpdateTimeCheckerLogicTrue(){
        val mockedSharedPref = mock(SharedPreferences::class.java)
        val editor = mock(SharedPreferences.Editor::class.java)

        //`when`(mockedSharedPref.getLong("teamsUpdate",-1)).thenReturn(-1L)

        `when`(mockedSharedPref.getLong("teamsUpdate",-1L)).thenReturn(-1L)
        `when`(mockedSharedPref.edit()).thenReturn(editor)

        val updateTime = UpdateTimeChecker(mockedSharedPref)

        assertEquals(true, updateTime.updateTeamsList())
    }

    @Test
    fun testTeamsUpdateTimeCheckerLogicFalse(){
        val mockedSharedPref = mock(SharedPreferences::class.java)
        val editor = mock(SharedPreferences.Editor::class.java)
        val cTime = System.currentTimeMillis()

        `when`(mockedSharedPref.getLong("teamsUpdate",-1L)).thenReturn(cTime)
        `when`(mockedSharedPref.edit()).thenReturn(editor)

        val updateTime = UpdateTimeChecker(mockedSharedPref)

        assertEquals(false, updateTime.updateTeamsList())
    }

    @Test
    fun testPlayersUpdateTimeCheckerLogicTrue(){
        val mockedSharedPref = mock(SharedPreferences::class.java)
        val editor = mock(SharedPreferences.Editor::class.java)

        `when`(mockedSharedPref.getLong("playersUpdate12",-1L)).thenReturn(-1L)
        `when`(mockedSharedPref.edit()).thenReturn(editor)

        val updateTime = UpdateTimeChecker(mockedSharedPref)

        assertEquals(true, updateTime.updatePlayersList(12))
    }

    @Test
    fun testPlayersUpdateTimeCheckerLogicFalse(){
        val mockedSharedPref = mock(SharedPreferences::class.java)
        val editor = mock(SharedPreferences.Editor::class.java)
        val cTime = System.currentTimeMillis()
        val asdf = cTime
        val teamId = 134880
        `when`(mockedSharedPref.getLong("playersUpdate$teamId",-1L)).thenReturn(cTime)
        `when`(mockedSharedPref.edit()).thenReturn(editor)

        val updateTime = UpdateTimeChecker(mockedSharedPref)

        assertEquals(false, updateTime.updatePlayersList(teamId))
    }
    @Test
    fun testNewsUpdateTimeCheckerLogicTrue(){
        val mockedSharedPref = mock(SharedPreferences::class.java)
        val editor = mock(SharedPreferences.Editor::class.java)

        `when`(mockedSharedPref.getLong("resultsUpdate12",-1L)).thenReturn(-1L)
        `when`(mockedSharedPref.edit()).thenReturn(editor)

        val updateTime = UpdateTimeChecker(mockedSharedPref)

        assertEquals(true, updateTime.updateNewsList(12))
    }

    @Test
    fun testNewsUpdateTimeCheckerLogicFalse(){
        val mockedSharedPref = mock(SharedPreferences::class.java)
        val editor = mock(SharedPreferences.Editor::class.java)
        val cTime = System.currentTimeMillis()
        val asdf = cTime
        val teamId = 134880
        `when`(mockedSharedPref.getLong("resultsUpdate$teamId",-1L)).thenReturn(cTime)
        `when`(mockedSharedPref.edit()).thenReturn(editor)

        val updateTime = UpdateTimeChecker(mockedSharedPref)

        assertEquals(false, updateTime.updateNewsList(teamId))
    }
}
